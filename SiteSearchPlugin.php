<?php

namespace Craft;

/**
 * Class SiteSearchPlugin
 * @package Craft
 */
class SiteSearchPlugin extends BasePlugin
{
    /**
     * @return null|string
     */
    public function getName()
    {
        return Craft::t('Site Search');
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return '0.3';
    }

    /**
     * @return string
     */
    public function getDeveloper()
    {
        return 'Bart van Gennep';
    }

    /**
     * @return string
     */
    public function getDeveloperUrl()
    {
        return 'http://www.itmundi.nl';
    }

    /**
     * @return bool
     */
    public function hasCpSection()
    {
        return false;
    }

    /**
     * @return array
     */
    public function registerSiteRoutes()
    {
        return array(
            'sitesearch/autocomplete/(?P<term>\w+)' => array('action' => 'siteSearch/autoComplete'),
        );
    }
}
