# Craft Site Search Plugin

## Usage
GET on `/actions/siteSearch/autocomplete?term={term}` will return matching keywords from the searchindex if term has 3 or more characters

### Example config
Example config to be added in config/general.php

    'autocomplete' => array(
        'Entry' => array(
            'searchField' => 'title',
            'criteria' => array(
                'limit' => 20,
                'order' => 'score',
            ),
            'results' => array(
                'id' => 'id',
                'title' => 'title',
                'type' => ['type', 'name'],
            )
        ),
        'Category' => array(
            'searchField' => 'title',
            'criteria' => array(
                'order' => 'score',
            ),
            'results' => array(
                'id' => 'id',
                'title' => 'title',
                'type' => ['group', 'name'],
            )
        ),
    )

- key is the name of the ElementType
- searchField is the field the autocomplete term is checked against
- criteria are extra static criteria on the query such as limit and order
- results determine the resulting json, key is the key in the json, value is used to retrieve the data from the model

## Changelog

### 0.2
* Added settings for configuring the elements to search through
* Search with ElementCriteriaModel in stead of querying keywords

### 0.1
* Added route for autocomplete on keywords from the searchindex