<?php

namespace Craft;

/**
 * Contact Form controller.
 */
class SiteSearchController extends BaseController
{
    /**
     * Allows anonymous access to this controller's actions.
     *
     * @var array|bool
     */
    protected $allowAnonymous = true;

    /**
     * @param array $variables
     */
    public function actionAutoComplete(array $variables = array())
    {
        $term = $variables['term'];

        $results = craft()->siteSearch->getAutoCompleteResults($term);

        $this->returnJson($results);
    }
}
