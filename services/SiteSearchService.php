<?php

namespace Craft;

/**
 * Contact Form service.
 */
class SiteSearchService extends BaseApplicationComponent
{
    /**
     * @return array
     */
    private $defaultAutoCompleteSettings = array(
        'Entry' => array(
            'searchField' => 'title',
            'criteria' => array(
                'limit' => 20,
                'order' => 'score',
            ),
            'results' => array(
                'id' => 'id',
                'title' => 'title',
                'uri' => 'url',
                'type' => ['type', 'name'],
            )
        ),
        'Category' => array(
            'searchField' => 'title',
            'criteria' => array(
                'order' => 'score',
            ),
            'results' => array(
                'id' => 'id',
                'title' => 'title',
                'uri' => 'url',
                'type' => ['group', 'name'],
            )
        ),
    );

    /**
     * @param $term
     * @return array
     */
    public function getAutoCompleteResults($term)
    {
        $results = array();
        $settings = $this->getAutoCompleteSettings();

        foreach($settings as $elementType => $properties){
            if($this->validateAutoCompleteSettings($elementType, $properties) == false ) {
                continue;
            }
            $searchFields = $properties['searchField'];
            if(!is_array($searchFields)){
                $searchFields = [$searchFields];
            }
            foreach($searchFields as $searchField){
                $criteria = $this->prepareCriteria($searchField, $properties['criteria'], $term);
                $elements = $this->findAutoCompleteResults($elementType, $criteria);
                $formattedResults = $this->formatAutoCompleteResults($elements, $properties['results']);
                $results = array_merge($results, $formattedResults);
            }
        }
        return $results;
    }

    /**
     * @return array|mixed
     */
    private function getAutoCompleteSettings()
    {
        $settings = craft()->config->get('autocomplete');
        if(empty($settings) || !is_array($settings)) {
            $settings = $this->defaultAutoCompleteSettings;
        }
        return $settings;

    }

    /**
     * @param string $searchField
     * @param array $criteria
     * @param string $term
     * @return array
     */
    private function prepareCriteria($searchField, array $criteria, $term)
    {
        $criteria[$searchField] = '*' . $term . '*';
        return $criteria;
    }

    /**
     * @param BaseModel $element
     * @param array $resultCriteria
     * @return array
     */
    private function formatAutoCompleteResult(BaseModel $element, array $resultCriteria)
    {
        $result = [];
        foreach($resultCriteria as $key => $value) {
            if(is_array($value)){
                $tempValue = $element;
                foreach($value as $val) {
                    $tempValue = $tempValue->$val;
                }
                $result[$key] = $tempValue;
            } else {
                $result[$key] = $element->$value;
            }
        }
        return $result;
    }

    /**
     * @param string $elementType
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    private function findAutoCompleteResults($elementType, array $criteria)
    {
        return craft()->elements->getCriteria($elementType, $criteria)->find();
    }

    /**
     * @param BaseModel[] $elements
     * @param array $resultCriteria
     * @return array
     */
    private function formatAutoCompleteResults(array $elements, array $resultCriteria)
    {
        $results = [];
        foreach ($elements as $element) {
            $results[] = $this->formatAutoCompleteResult($element, $resultCriteria);
        }
        return $results;
    }

    /**
     * @param $elementType
     * @param $properties
     * @return bool
     */
    private function validateAutoCompleteSettings($elementType, $properties)
    {
        return true;
    }
}

